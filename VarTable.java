import java.util.ArrayList;


public class VarTable {
  private ArrayList<String> names;
  private ArrayList<Returned> values;

  public VarTable(){
    this.names = new ArrayList<>();
    this.values = new ArrayList<>();
  }

  public void insertVar(String name, Returned val){
    for(int i=0;i<names.size();i++){
      if(names.get(i).equals(name)) {
	values.set(i, val);   //overwrite already existing variable
        return;
      }
    }

    //add new variable 
    names.add(name);
    values.add(val);
  }

  public Returned getVarValue(String name){
    for(int i=0;i<names.size();i++){
      if(names.get(i).equals(name)) return values.get(i);
    }
    throw new SemError("Unknown variable : "+name);
  }
}
