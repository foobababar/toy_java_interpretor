/*
 *
 * Provide handmade methods and tricks to manipulate Strings
 * 
 *
 */

import java.util.ArrayList;

public class WordProcessor {
  private String in;
  private String out;
  private int count;

  public WordProcessor(String fileName){
    //in = "    x = 0 ;\n \n print x ; \n  \n    \n "; 
    in = Util.readFile(fileName);
    count = 0;
  }

  /*
   *
   * Returns array of all Strings in <in>
   *
   */
  public ArrayList<String> getWords(){
    ArrayList<String> out = new ArrayList<>(); 
    String word = "";
    while(count < in.length()){
      if(in.charAt(count) == ' ' || in.charAt(count)=='\n'   ){
        if(! word.equals(" ")  && ! word.equals("") && ! word.equals("\n")){
	  out.add(word);
	  word = "";
	}
      }

      else  word+=in.charAt(count);

      count++;
    }

    return out;
  }
}
