import java.util.ArrayList;

/*
 * in : String input
 *
 * out : String output 
 *
 * 1) analyze input
 * 2) recognize `import std ;`
 * 3) replace with fns of std
 * 
 */

public class Prepro {
  private String input;
  private String output;

  private ArrayList<Token> tokLst;

  public Prepro(String input){
    this.input = input;
  }


  public void load(){
    /*
     *  input -> tokLst
     */
    Scanner s = new Scanner(input);
    tokLst = s.getTokens();
  }

  public void substitute(){
    /*
     *
     * replace `import std ; ` 
     * with fns of std
     *
     */
    String rawStd = Util.readFile("std.txt");
    output+=rawStd;
  }

  public void dump(String name){
    /*
     * dump tokLst in  
     * out.txt 
     *
     */
    output = "";
    substitute();
    for(int i=0;i<tokLst.size();i++){
      //System.out.println("Dumping -> "+tokLst.get(i).getTokType());
      switch(tokLst.get(i).getTokType()){
	case "LBRACK":
	  output+="{\n";
	  break;
	case "RBRACK":
	  output+="}\n";
	  break;
	case "LPAR":
	  output+="(";
	  break;
	case "RPAR": 
	  output+=") ";
	  break;
	case "COMMA":
	  output+=", ";
	  break;
	case "PRINT":
	  output+="print ";
	  break;
	case "FN":
	  output+="fn ";
	  break;
	case "CALL":
	  output+="call ";
	  break;
	case "RETURN":
	  output+="return ";
	  break;
	case "IF":
	  output+="if";
	  break;
	case "WHILE":
	  output+="while";
	  break;
  	case "EXIT" :
	  output+="exit";
	  break;
	case "SHOWTABLE" :
	  output+="showtable";
	  break;	
	case "SEP":
	  output+=";\n";
	  break;
	case "ASS":
	  output+=" = ";
	  break;
	case "NB":
	  output+=tokLst.get(i).getTokValue();
	  output+=" ";
	  break;
	case "ID":
	  output+=tokLst.get(i).getTokValue();
	  break;
	case "OP":
	  output+=tokLst.get(i).getTokValue();
	  break;
	case "EOF":
	  break;
	default:
	  throw new RuntimeException("UNREACHABLE : "+tokLst.get(i).getTokType());
      }
    }
    Util.writeToFile(name, output);
  }
}
