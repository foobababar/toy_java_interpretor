import java.util.ArrayList;

public class Main {
  public static void main(String[] args){
    boolean doParse = true;
    boolean doEvaluate = true;

    System.out.println("\n______________________________________________\n");


    System.out.println("\n\n######## Import file ########\n\n");
    if(args.length != 1 ){
      throw new RuntimeException("usage : java Main programName.txt");
    } 
    String prog = Util.readFile(args[0]); 


    System.out.println("\n\n######## SCANNING ########\n\n");
    Scanner scanner = new Scanner(prog);
    ArrayList<Token> tokLst = scanner.getTokens();
    
    for(int i=0;i<tokLst.size();i++){
      System.out.println("Found token "+tokLst.get(i).getTokType()+" with value : "+tokLst.get(i).getTokValue());
    }

    if(!doParse) return;

    System.out.println("\n\n######## PARSING ########\n\n");
    Parser parser = new Parser(tokLst);
    Node tree = parser.parse();


    if(!doEvaluate) return;

    System.out.println("\n\n######## EVALUATING ########\n\n");

    Evaluator eval = new Evaluator(tree);
    eval.evaluate();
    
  }
}
