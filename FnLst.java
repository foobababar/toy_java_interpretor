/*
 *
 * Contains list of declared functions + signatures
 *
 *
 * */

import java.util.ArrayList;

public class FnLst {
  private ArrayList<String> names;
  private ArrayList<ArrayList<String>> parameters;
  private ArrayList<Node> nodes;

  public FnLst(){
    names = new ArrayList<>(); /*  names  of all fn */
    parameters = new ArrayList<>(); /*  list of all parameters of all fn */
    nodes = new ArrayList<>();  /*  nodes of all fn */
  }

  /*
   * Get parameters of fn <name>
   * 
   */
  public ArrayList<String> getParameters(String name){  //get parameters of fn <name>
    for(int i=0;i<names.size();i++){
      if(names.get(i).equals(name)) return parameters.get(i);
    }
    throw new SemError("Unknown fn "+name);
  }

  /*
   * get (L)Node of fn <name>
   *
   * */

  public Node getNode(String name){  //get node of fn <name>
    for(int i=0;i<names.size();i++){
      if(names.get(i).equals(name)) return nodes.get(i);
    }
    throw new SemError("Unknown fn "+name);
  }


/*     setters & getters      */
  public ArrayList<String> getNames(){
    return this.names;
  }

  public ArrayList<ArrayList<String>> getParameters(){
    return this.parameters;
  }

  public ArrayList<Node> getNodes(){
    return this.nodes;
  }




  public Integer fnExists(String name){
    for(int i=0;i<names.size();i++){
      if(names.get(i).equals(name)) return i;
    }
    return null;
  }


  /* 
   *
   * add new fn to fnLst
   * or 
   * overwrite existing fn
   * 
   * same fn names with diff signatures not authorized
   *
   * */

  public void addFn(String name, ArrayList<String> params, Node node){
    Integer index = fnExists(name);
    if(index==null){    /* fn <name> doesn't exist yet : create it */
      names.add(name);
      parameters.add(params);
      nodes.add(node);
    }
    else { 			/* fn <name> already exists : overwrite it */

      /* check if same number of parameters. If not, throw err */
      if(parameters.get(index).size() != params.size()) throw new SemError("Wrong nb of parameters for redefined fn ||"+name+"||");

      parameters.set(index, params);
      nodes.set(index, node);
    }
  }

  /*
   * pretty print FnTable
  public void showFnTable(){
    System.out.println("*** FnTable ***");
    System.out.println("--------------------------");
    for(int i=0;i<names.size(); i++){
      System.out.print(names.get(i)+"       |       ");
      for(int j = 0;j<parameters.get(i).size();j++){
        System.out.print(parameters.get(i).get(j)+" ");
      }
      System.out.println("\n--------------------------");
    }
  }

  */
}
