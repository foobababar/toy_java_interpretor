import java.util.ArrayList;

public class Test {
  public static void main(String []args){

    Scanner scan = new Scanner(Util.readFile(args[0]));
    ArrayList<Token> tokLst = scan.getTokens();

    for(int i=0;i<tokLst.size();i++){
      System.out.println("Found token "+tokLst.get(i).getTokType()+" with value : "+tokLst.get(i).getTokValue());
    }
  }
}
