/*
 *
 *
 *
 *  Leafs are dead ends
 *  They just hold a type and a value
 *
 *
 *
 *
 * */

public class Leaf extends Node {
  public Leaf(String type, String value){
    super();
    this.type = type;
    this.value = value;
    this.isLeaf = true;
  }
}
