import java.io.File; 
import java.io.FileNotFoundException;
import java.util.Scanner;

import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
//file writer




public class Util {
  public static void p(String message){
    System.out.println(message);
  }
  
  public static String readFile(String filename) {
    String text = "";
    try {
      File myObj = new File(filename);
      Scanner myReader = new Scanner(myObj);
      while (myReader.hasNextLine()) {
        String data = myReader.nextLine();
	text+=(data+"\n");
      }
      myReader.close();
    } catch (FileNotFoundException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
    return text;
  }

  public static void writeToFile(String filename, String mess){
    File file = new File(filename);
    try {
      FileWriter f = new FileWriter(file);
      f.write(mess);
      f.close();
    }
    catch(FileNotFoundException e){
      e.printStackTrace();
    }
    catch(IOException e){
      e.printStackTrace();
    }
  }

  public static float string2float(String st){
    return Float.parseFloat(st);
  }

  public static Float string2Float(String st){
    return Float.valueOf(st);
  }

  public static String float2String(Float f){
    return Float.toString(f);
  }

  public static String int2String(int i){
    return Integer.toString(i);
  }

  public static int string2int(String s){
    return Integer.parseInt(s);
  }
  
  public static Integer tryInt(String s){
    try {
      return Integer.parseInt(s);
    }catch (java.lang.NumberFormatException e){
      return null; 
    }
  }


  public static Integer tryInt(int s){
    return s;
  }





  /*
   *
   *  Compare <left> with <right> using op <op>
   *  returns true if test is ok 
   *  else returns false
   *  
   *  throw error if unknown op
   */

  public static boolean compare(int left, String op, int right){
      switch(op){
	case ">":
	  if(left > right)   return true;
	  else return false;
	case "<":
	  if(left < right)   return true;
	  else return false;
        case "==":
	  if(left == right)  return true; 
          else return false;
 	case "!=":
	  if(left != right)   return true;
	  else return false;
	case ">=":
	  if(left >= right)   return true; 
	  else return false; 
	case "<=":
	  if(left <= right)   return true;
	  else return false;
	default : 
	  throw new SemError();
    }
  }


}
