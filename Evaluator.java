import java.util.ArrayList;


public class Evaluator {
  private Node tree;
  private FnLst fnLst;

  public Evaluator(Node tree){
    this.tree = tree;
    this.fnLst = new FnLst();
  }

  public void evaluate(){
    System.out.println("Evaluating ...");

    VarTable varTable = new VarTable();
    LEvaluate(tree, varTable);
  }


  /*
   *
   * L returns a value only when encountering return <id>
   * else it will return null
   *
   * varTable is the context of the block, IE all variables known 
   * to LEvaluate
   *
   */ 

/////////////////////////////////   L   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  
  public Returned LEvaluate(Node node, VarTable varTable){
    if(node.type.equals("EOF"))  System.out.println("Eval OK");  /* we reached EOF : eval successful */
    
    else if(node.type.equals("EOB")){ 
      /* 
       * EOB = end of bracket. We reached '}', end of bracket block. 
       * 
       * while block, if block, fn(def) block
       * will all end here.
       *
       */
	return null;
    }
    else { 

      
      Returned ret1 = IEvaluate(node.left, varTable);
      if(ret1!=null)  return ret1;  // we have return value 
      
      Returned ret2 = LEvaluate(node.right, varTable);
      if(ret2!=null)  return ret2;  // we have return value
      
    }

    return null; /*  default return value if ((no ret1) and (no ret2)) or EOF  */
  }
  

/*
 * if node.type is return, return node.value
 * else return null
 *
 */

/////////////////////////////////   I   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  
  public Returned IEvaluate(Node node, VarTable varTable){

///////  id  =  E   \\\\\\\\
    if(node.type.equals("ASS")) {
      varTable.insertVar(node.left.value, EEvaluate(node.right, varTable));
    }


///////  print id  \\\\\\\\
    else if(node.type.equals("PRINT"))  {
      Returned ret = EEvaluate(node.left, varTable);
      if(ret.returnedInt != null) System.out.println(ret.returnedInt); 
      else System.out.println(ret.returnedString);
    }


///////  fn def : `fn foo(){...}`  \\\\\\\\
    else if(node.type.equals("FNDEF")) {
      fnLst.addFn(node.left.value, node.declaredParameters, node.getMid());
    }


///////  return id  \\\\\\\\
    else if(node.type.equals("RETURN")){
      //return this or return null
      return EEvaluate(node.left, varTable);
    }

    

///////   if   \\\\\\\\
    else if(node.type.equals("IF")){
      String op = node.value;

      int left = EEvaluate(node.left, varTable).returnedInt;
      int right = EEvaluate(node.right, varTable).returnedInt;

      if(Util.compare(left, op, right)){   /* if <left> <op> <right> */

	/* return value inside if */
        Returned ret = LEvaluate(node.getMid(), varTable);
        if(ret!=null)  return ret;  /* we have ret value */
      }
    }



///////   while   \\\\\\\\
    else if(node.type.equals("WHILE")){
      String op = node.value;
      while (Util.compare(EEvaluate(node.left, varTable).returnedInt, op, EEvaluate(node.right, varTable).returnedInt)) {  /* while <left> <op> <right> */

	/* return value inside while. Put here or else infinite loop */
	Returned ret = LEvaluate(node.getMid(), varTable);
	if(ret != null)  return ret;  /* we have ret value */
      }
    }


///////   fncall solo   \\\\\\\\
    else if(node.type.equals("FNCALL")) {
      ArrayList<String> declaredParameters = fnLst.getParameters(node.left.value); //get list of parameters from FnLst
      ArrayList<Node> passedParameters = node.passedParameters;
      Node nodeToEval = fnLst.getNode(node.left.value);


      //TODO : check for diff nb of parameters in declaredParameters and passedParameters

      VarTable newVarTable = new VarTable();

      for(int i=0;i<declaredParameters.size(); i++){
        newVarTable.insertVar(declaredParameters.get(i), EEvaluate(passedParameters.get(i), varTable));
      }

      LEvaluate(nodeToEval, newVarTable);

    }



//////  unknown node type  \\\\\\
    else throw new SemError("Unknown node type : "+node.type);


    return null;  /* default return for IEvaluate */
  }



  /////////////////////////////////   E   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  
  public Returned EEvaluate(Node node, VarTable varTable){
    if(node.type.equals("TONLY"))   return TEvaluate(node.left, varTable);

    else if (node.type.equals("OP+"))   {
      //operation ok for int + int or string + string

      Returned left = TEvaluate(node.left, varTable);
      Returned right = EEvaluate(node.right, varTable);

      if(left.returnedInt != null && right.returnedInt != null)   //check if types are compatibles (int + int)
	return new Returned(left.returnedInt + right.returnedInt);  //return calculated value
      
      else if(left.returnedString != null && right.returnedString != null) //check if types are compatibles (string + string)
	return new Returned(left.returnedString + right.returnedString); //return concatenated string

      else 
	throw new SemError("Incompatible types");

    }
    else if (node.type.equals("OP-"))    {
      Returned left = TEvaluate(node.left, varTable);
      Returned right = EEvaluate(node.right, varTable);

      if(left.returnedInt != null && right.returnedInt != null)  //check if types are compatibles (string - string)
	return new Returned(left.returnedInt - right.returnedInt);      

      else 
	throw new SemError("Incompatible types");
    }
    else throw new SemError();
  }


/////////////////////////////////   T    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  
  public Returned TEvaluate(Node node, VarTable varTable){
    if(node.type.equals("FONLY"))  return FEvaluate(node.left, varTable);
    else if (node.type.equals("OP*")){
      Returned left = FEvaluate(node.left, varTable);
      Returned right = TEvaluate(node.right, varTable);

      if(left.returnedInt != null && right.returnedInt != null)  //check if types compatibles
	return new Returned(left.returnedInt * right.returnedInt);      
      
      else 
	throw new SemError("Incompatible types");
    }
    else if (node.type.equals("OP/")){

      Returned left = FEvaluate(node.left, varTable);
      Returned right = TEvaluate(node.right, varTable);

      if(left.returnedInt != null && right.returnedInt != null) //check if types compatibles
	return new Returned(left.returnedInt / right.returnedInt);      

      else 
	throw new SemError("Incompatible types");
    }
    else throw new SemError();
  }


/////////////////////////////////   F   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  
  public Returned FEvaluate(Node node, VarTable varTable){

    if(node.type.equals("ID")){  /* substitute variable <id> with its value from varTable */
      return varTable.getVarValue(node.value);
    }
    else if(node.type.equals("NB")){
      return new Returned(Integer.parseInt(node.value)); 
    }
    else if(node.type.equals("(E)")){
      return EEvaluate(node.left, varTable);  
    }

    else if(node.type.equals("FNCALL")){
      ArrayList<String> declaredParameters = fnLst.getParameters(node.right.value); //get list of parameters from FnLst
      ArrayList<Node> passedParameters = node.passedParameters;
      Node nodeToEval = fnLst.getNode(node.right.value);

      //TODO : check for diff nb of parameters in declaredParameters and passedParameters
      
      VarTable newVarTable = new VarTable();

      for(int i=0;i<declaredParameters.size(); i++){
        newVarTable.insertVar(declaredParameters.get(i), EEvaluate(passedParameters.get(i), varTable));
      }

      Returned ret = LEvaluate(nodeToEval, newVarTable);
      if(ret != null) return ret;
      else throw new SemError("Need return value for fn :...");

    }
    else if(node.type.equals("STRING")){
      return new Returned(node.value);
    }

    else throw new SemError("(from FEvaluate) Expected node of type 'ID' 'NB' or '(E)' but got : "+node.type);
  }
}
