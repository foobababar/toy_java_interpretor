/*
 * return value from FEvaluate
 * can be int or String
 *
 */


public class Returned {
  Integer returnedInt;
  String returnedString;
  
  public Returned(String value){
    returnedString = value;
  }

  public Returned(Integer value){
    returnedInt = value;
  }

}
