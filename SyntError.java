/*
 *
 *  Syntax error
 *  
 *  thrown by Parser
 *
 *
 * */


public class SyntError extends RuntimeException {
  public SyntError(){
    super("UNREACHABLE");
  }
  public SyntError(String errMess){
    super(errMess);
  }
  public SyntError(String expected, String type, String value){
    super("Expected token ||"+expected+"|| but found token ||"+type+"|| with value ||"+value+"||");
  }

  public SyntError(String expected, String type, int value){
    super("Expected token ||"+expected+"|| but found token ||"+type+"|| with value ||"+value+"||");
  }
}
