/*
 *
 * 
 * Lexical error
 *
 * thrown by Scanner
 *
 *
 * */



public class LexError extends RuntimeException {
  public LexError(String message){
    super(message);
  }
}
