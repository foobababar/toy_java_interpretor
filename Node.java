import java.util.ArrayList;


public class Node {

////  attributes for all nodes  \\\\
  String type;  //ID with unique value for evaluation (like ASS, CALLASS, OP+, etc.) 
  String value; //holds value for maths (like foo, 72, etc.)

  Node left;
  Node right;

  public boolean isLeaf;

////  attributes for fnNodes  \\\\
  private Node mid;

  ArrayList<String> declaredParameters;
  ArrayList<Node> passedParameters;


////  CONSTRUCTOR  \\\\
  public Node(){

    this.declaredParameters = new ArrayList<>();
    this.passedParameters = new ArrayList<>();
    this.isLeaf = false;
  }

  
////  setters and getters for fnNodes  \\\\
  public Node getMid(){
    return this.mid;
  }
  public void setMid(Node mid){
    this.mid = mid;
  }
}
