public class Token {
  private String tokType;
  private String tokValue;

  public Token(String tokType, String tokValue){
    this.tokType = tokType;
    this.tokValue = tokValue;
  }

  public String getTokType(){
    return this.tokType;
  }

  public void setTokType(String newType){
    this.tokType = newType;
  }

  public String getTokValue(){
    return this.tokValue;
  }

  public void setTokValue(String newValue){
    this.tokValue = newValue;
  }
}
