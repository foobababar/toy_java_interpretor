import java.util.ArrayList;

public class Scanner {
  private String program;
  private Changeable scanCount; 
  private ArrayList<Token> toklst; 

  public Scanner(String program){
    this.program = program;
    this.scanCount = new Changeable();
    this.toklst = new ArrayList<>();
  }

  Changeable getScanCount(){
    return this.scanCount ;
  }
  
  Token scan(){
    
    if (scanCount.value>=program.length()) return new Token("EOF", "NONE");
    
    switch (program.charAt(scanCount.value)){ 
      case ' ':
      case '\n':
        while (scanCount.value<program.length() &&  (program.charAt(scanCount.value) == ' ' || program.charAt(scanCount.value)=='\n')){
          scanCount.increment();
        }
        return new Token("EMPTY", "NONE");
      
      case '#':
        while (program.charAt(scanCount.value) != '\n' ) {
          scanCount.increment();
        }
        scanCount.increment();
        return new Token("EMPTY", "NONE");


     
      case '+':
      case '-':
      case '*':
      case '/':
        char op=program.charAt(scanCount.value);
        scanCount.increment();
        return new Token("OP", Character.toString(op));

      case '!':
        scanCount.increment();
        if (program.charAt(scanCount.value) == '='){
          scanCount.increment();
          return new Token("OPC", "!=");
        }
        else throw new LexError("Expected '=' after '!' but got : "+program.charAt(scanCount.value));

      case '<':
        scanCount.increment();
        if (program.charAt(scanCount.value) == '='){
          scanCount.increment();
          return new Token("OPC", "<=");
        }
        else return new Token("OPC", "<");
        
      case '>':
        scanCount.increment();
        if (program.charAt(scanCount.value) == '='){
          scanCount.increment();
          return new Token("OPC", ">=");
        }
        else return new Token("OPC", ">");

      case ';':
	scanCount.increment();
	return new Token("SEP", "NONE");
      
      case ',':
	scanCount.increment();
	return new Token("COMMA", "NONE");

      case '(':
	scanCount.increment();
	return new Token("LPAR", "NONE");

      case ')':
	scanCount.increment();
	return new Token("RPAR", "NONE");

      case '{':
	scanCount.increment();
	return new Token("LBRACK", "NONE");
      
      case '}':
	scanCount.increment();
	return new Token("RBRACK", "NONE");
      
      case '=':
        scanCount.increment();
        if (program.charAt(scanCount.value) == '='){
          scanCount.increment();
          return new Token("OPC", "==");
        }
        else return new Token("ASS", "NONE");

      case '"' :
	String buff ="";
	scanCount.increment();
	while (program.charAt(scanCount.value) != '"'){
	  buff+=program.charAt(scanCount.value) ;
	  scanCount.increment();
	  if(scanCount.value>=program.length()) throw new LexError("Reached end of file while scanning string. Don't forget to add \" to close your strings !");
	}
	scanCount.increment();
	return new Token("STRING", buff);
      
      default :  //////  ID or Keyword or NB  \\\\\\
        if (Character.isDigit(program.charAt(scanCount.value))  ||  program.charAt(scanCount.value)=='-'  ){   // NB 
          String buf="";
	  if(program.charAt(scanCount.value)=='-'){
	    buf+="-";
	    scanCount.increment();
	  }
          while (   Character.isDigit(program.charAt(scanCount.value))){
            buf+=program.charAt(scanCount.value);
            scanCount.increment();
          }
          return new Token("NB", buf);
        }
        
	else if (Character.isLetter(program.charAt(scanCount.value))){    //ID 
          String buf = "";
          while (  Character.isLetter(program.charAt(scanCount.value)) ||  Character.isDigit(program.charAt(scanCount.value))  ){
            buf+=program.charAt(scanCount.value);
            scanCount.increment();
          }
	  
	  //keywords 
          if(buf.equals("print")) return new Token("PRINT", "NONE");
          else if(buf.equals("fn")) return new Token("FN", "NONE");
          else if(buf.equals("call")) return new Token("CALL", "NONE");
          else if(buf.equals("return")) return new Token("RETURN", "NONE");
          else if(buf.equals("if")) return new Token("IF", "NONE");
	  else if(buf.equals("while")) return new Token("WHILE", "NONE");  
	  else if(buf.equals("exit")) return new Token("EXIT", "NONE");    
	  else if(buf.equals("showTable")) return new Token("SHOWTABLE", "NONE");    

	  //prepro
	  else if(buf.equals("import")) return new Token("IMPORT", "NONE");    

	  //ID
	  else return new Token("ID", buf);
        } 
        else   throw new LexError("Unknown token '"+program.charAt(scanCount.value)+"'"  );
    }
  }
  
  ArrayList<Token> getTokens(){
    while (scanCount.value < program.length()) {
      Token tok = this.scan();
      
      if (tok.getTokType()!="EMPTY") { 
	toklst.add(tok);
      }
    }

    //Add EOF at end of toklst if not there already\\
    if (!toklst.get(toklst.size()-1).getTokType().equals("EOF")) toklst.add(new Token("EOF", "NONE")); 
    return this.toklst;
  }
}
