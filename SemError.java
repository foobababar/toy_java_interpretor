/*
 *  Semantic errors 
 *  
 *  thrown by Evaluator  
 *
 *
 * */



public class SemError extends RuntimeException {
  public SemError(){
    super("UNREACHABLE");
  }

  public SemError(String message){
    super(message);
  }

  public SemError(String expected, String type, String value){
    super("Expected token ||"+expected+"|| but found token ||"+type+"|| with value ||"+value+"||");
  }

  public SemError(String expected, String type, int value){
    super("Expected token ||"+expected+"|| but found token ||"+type+"|| with value ||"+value+"||");
  }
}
