import java.util.ArrayList;


public class Parser {
  private Changeable parseCount;
  private ArrayList<Token> tokLst;

  public Parser(ArrayList<Token> tokLst){
    this.parseCount = new Changeable();
    this.tokLst = tokLst;
  }


/////////////////////////////////   Start of helper methods   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  


  public boolean checkTok(String type){
    if(tokLst.get(parseCount.value).getTokType().equals(type)) return true;
    return false;
  }

  public String getCurrTokType(){
    return tokLst.get(parseCount.value).getTokType();
  } 

  public String getCurrTokValue(){
    return tokLst.get(parseCount.value).getTokValue();
  } 


  /*
   * Use this method for fn declaration. Allows only IDs.
   *
   */
  public void checkFnDeclaredParameters(Node node){
    if(checkTok("ID")){
      node.declaredParameters.add(getCurrTokValue());
      parseCount.increment();
      if(checkTok("COMMA")){
        parseCount.increment();
	checkFnDeclaredParameters(node);
      }
    }
  }

  /*
   *
   * Use this method to handle passed arguments to function call.
   *
   */

  public void checkFnPassedParameters(Node node){
      if(checkTok("RPAR")) return;
      node.passedParameters.add(E());

      if(checkTok("COMMA")){
        parseCount.increment();
	checkFnPassedParameters(node);
      }
  }





/////////////////////////////////   End of helper methods   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  


  public Node parse(){
    System.out.println("Parsing ...");
    return L();   /* entrypoint */
  }


/////////////////////////////////   L -> I;L | eof   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  


  public Node L(){
    Node node = new Node();
    node.type = "NONE";

    if(checkTok("EOF")){
      System.out.println("Parsing successful");
      node.type = "EOF";
      parseCount.increment();
      return node;
    }

    else if(checkTok("RBRACK")) {
      /*
       * while block, if block, fn(def) block
       * will all end here.
       */
      node.type = "EOB"; /* end of bracket block. Incremented at (*1)  */
      return node;
    }

    node.left = I();
    
    if(checkTok("SEP")) parseCount.increment();
    else if(! checkTok("SEP") && tokLst.get(parseCount.value-1).getTokType().equals("RBRACK")) {} /* this line allows no ';' after fndef */
    
    /* helpful error message if forgot 'call' in `id = call bar()`; */
    else if( checkTok("LPAR")) throw new SyntError("Missing keyword 'call' before : "+tokLst.get(parseCount.value-1).getTokValue());
    else throw new SyntError("Expected ';' but got "+getCurrTokType());
    
    node.right = L();

    return node;
  }



/////////////////////////////////   I -> id = E |  id = foo() | foo() | print E | fn foo(){...} | return E | \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  
  public Node I(){
    Node node = new Node();

////  id = E  \\\\
    if(checkTok("ID")){ 
      
      /* check for id token */
      node.left = new Leaf("ID", getCurrTokValue());
      parseCount.increment();
      
      /* check for '=' token */
      if(checkTok("ASS")) {
        parseCount.increment();
        node.type = "ASS";
        node.right = E();
      }
      else if(checkTok("LPAR")){
        parseCount.increment();
	node.type = "FNCALL";

	checkFnPassedParameters(node);

	if(checkTok("RPAR")) parseCount.increment();
	else throw new SyntError("Expected RPAR after fncall solo");
      }


      else throw new SyntError("Expected ASS but got "+getCurrTokType());
    }


//// print E \\\\
    else if(checkTok("PRINT")){
      node.type = "PRINT";
      parseCount.increment();

      node.left = E();

    }
    
    
//// fndef : `fn foo(){...}` \\\\

    else if(checkTok("FN")){
      node.type = "FNDEF";
      parseCount.increment();

      if (checkTok("ID")){
        node.left = new Leaf("ID", getCurrTokValue());
	parseCount.increment();
	
        if(checkTok("LPAR")) parseCount.increment();
        else throw new SyntError("LPAR (FNDEF)", getCurrTokType(), getCurrTokValue());
    
	checkFnDeclaredParameters(node);
	
        if(checkTok("RPAR")) parseCount.increment();
        else throw new SyntError("RPAR (FNDEF)", getCurrTokType(), getCurrTokValue());


	if(checkTok("LBRACK"))parseCount.increment();
        else throw new SyntError("LBRACK", getCurrTokType(), getCurrTokValue());

	/* keep fn body in node.mid */
	node.setMid(L());

	if(checkTok("RBRACK"))parseCount.increment();  /*  (*1)  */
        else throw new SyntError("RBRACK", getCurrTokType(), getCurrTokValue());
      }
      else throw new SyntError("ID (after FNDEF)", getCurrTokType(), getCurrTokValue());
    }

    
//// return E  \\\\
    else if(checkTok("RETURN")){
      node.type = "RETURN";
      parseCount.increment();

      node.left = E();
    }


//// if \\\\
    else if(checkTok("IF")){
      node.type = "IF";
      parseCount.increment();
      
      if(checkTok("LPAR")) parseCount.increment();
      else  throw new SyntError("LPAR (after if)", getCurrTokType(), getCurrTokValue());

      //check condition
      node.left = E();

      ///check opc
      if(checkTok("OPC")){
        node.value = getCurrTokValue();
	parseCount.increment();
      } 
      else  throw new SyntError("OPC (after if)", getCurrTokType(), getCurrTokValue());

      node.right = E();

      if(checkTok("RPAR")) parseCount.increment();
      else  throw new SyntError("RPAR (after if)", getCurrTokType(), getCurrTokValue());

      if(checkTok("LBRACK")) parseCount.increment();
      else  throw new SyntError("LBRACK (after if)", getCurrTokType(), getCurrTokValue());

      /* LEvaluate */
      node.setMid(L());

      if(checkTok("RBRACK")) parseCount.increment();  /* (*1) */
      else  throw new SyntError("RBRACK (after if)", getCurrTokType(), getCurrTokValue());
    }

//// while \\\\
    else if(checkTok("WHILE")){
      node.type = "WHILE";
      parseCount.increment();
      
      if(checkTok("LPAR")) parseCount.increment();
      else  throw new SyntError("LPAR (after while)", getCurrTokType(), getCurrTokValue());

      //check condition

      node.left = E();

      ///check opc
      if(checkTok("OPC")){
        node.value = getCurrTokValue();
	parseCount.increment();
      } else  throw new SyntError("OPC (after while)", getCurrTokType(), getCurrTokValue());

      node.right = E();

      if(checkTok("RPAR")) parseCount.increment();
      else  throw new SyntError("RPAR (after while)", getCurrTokType(), getCurrTokValue());

      if(checkTok("LBRACK")) parseCount.increment();
      else  throw new SyntError("LBRACK (after while)", getCurrTokType(), getCurrTokValue());

      /* LEvaluate */
      node.setMid(L());

      if(checkTok("RBRACK")) parseCount.increment();  /* (*1) */
      else  throw new SyntError("RBRACK (after while)", getCurrTokType(), getCurrTokValue());
    }

    else throw new SyntError("ID = E or FNDEF or FNCALL or RETURN or PRINT", getCurrTokType(), getCurrTokValue());

    return node;
  }


/////////////////////////////////   E -> T+E | T-E | T  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  
/*
 *
 * + and - are operators with lowest priority : they are higher in grammar
 *
 *
 */
  public Node E(){
    Node node = new Node();
    node.left = T();
    node.type = "TONLY";

    if(checkTok("OP")){
      if(getCurrTokValue().equals("+") || getCurrTokValue().equals("-") ){ 	
	node.type = "OP"+getCurrTokValue();
        node.value = getCurrTokValue(); 
        parseCount.increment();

	node.right = E();
      }
      else throw new SyntError(); /* can't be other op. Throw error */
    }

    return node;
  }


/////////////////////////////////   T -> F*T | F/T | F  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  

/*
 *
 * / and * have higher priority than + and - so they are lower in grammar
 *
 */
  public Node T(){
    Node node = new Node();
    node.left = F();
    node.type = "FONLY";

    if(checkTok("OP")){
      if(getCurrTokValue().equals("*") || getCurrTokValue().equals("/") ){ 	
	node.type = "OP"+getCurrTokValue();
        node.value = getCurrTokValue(); 
        parseCount.increment();
	node.right = T();
      }
      else {} /* can be other ops, don't throw err */
    }

    return node;
  }


/////////////////////////////////   F -> id | nb | (E)  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  
/*
 *
 * LPAR has higher priority, so it is lower in grammar
 * 
 *
 *
 */
  public Node F(){
    Node node = new Node();

    if(checkTok("ID")){
      
      //id solo 
        node = new Leaf("ID", getCurrTokValue());      

	//save token value for later
        String tmpValue = getCurrTokValue();
	parseCount.increment();


      //fncall
      if(checkTok("LPAR")) {
	//reset node 
	node = new Node();

	//saved token value from before
        node.right = new Leaf("ID", tmpValue);

	node.type = "FNCALL";
	parseCount.increment();
        
	checkFnPassedParameters(node);

	if(checkTok("RPAR")) parseCount.increment();
        else throw new SyntError("RPAR", getCurrTokType(), getCurrTokValue());
      }
    }

    else if(checkTok("NB")){ 
      node = new Leaf("NB", getCurrTokValue());
      parseCount.increment();
    }
    
    else if (checkTok("LPAR")){
      node.type = "(E)";
      parseCount.increment();
      node.left = E();

      if(checkTok("RPAR")) parseCount.increment();
      else throw new SyntError("RPAR", getCurrTokType(), getCurrTokValue());
    }

    else if(checkTok("STRING")){

      node = new Leaf("STRING", getCurrTokValue());
      parseCount.increment();

    }

    else throw new SyntError("Unknown token "+getCurrTokType());
    return node;
  }
}
